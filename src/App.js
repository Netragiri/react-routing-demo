import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from './Components/Home';
import User from './Components/User';
import NavBar from './Components/NavBar';
import UserId from './Components/UserId';
import PageNotFound from './Components/PageNotFound';
import Products from './Components/Products';
import NewProduct from './Components/NewProduct';
import OldProducts from './Components/OldProducts';
import NewChild from './Components/NewChild';

function App() {
  return (
    <div>
      <NavBar />
      
          <Routes>
    
          <Route path='' element={<Home />}></Route>
          <Route path='user' element={<User />}>
          <Route path=':userid' element={<UserId />}></Route>
          <Route path='netra' element={<UserId />}></Route>
            </Route>
          
          <Route path='products' element={<Products />}>
            <Route path='newProduct' element={<NewProduct />}>
              <Route path='newchild' element={<NewChild />}></Route>
              </Route>
            <Route path='oldProducts' element={<OldProducts />}></Route>
            </Route>

          <Route path='*' element={<PageNotFound />}></Route>
 
          </Routes>
      
    </div>
  );
}

export default App;
