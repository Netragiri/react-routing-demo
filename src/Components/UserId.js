import React from 'react'
import { useParams } from 'react-router-dom'

function UserId() {
    let params=useParams()
  return (
    <div>
      <h1>hello {params.userid}</h1>
    </div>
  )
}

export default UserId
