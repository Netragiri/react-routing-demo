import React from 'react'
import { Outlet } from 'react-router-dom'

function NewProduct() {
  return (
    <div>
      <h4>new products</h4>
      <Outlet />
    </div>
  )
}

export default NewProduct
