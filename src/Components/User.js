import React from 'react'
import { useSearchParams } from 'react-router-dom'

function User() {
  const [searchparams,setSearchParams]=useSearchParams()
  const Activeusers=searchparams.get('users')=='Active'
  return (
    <div>
      <h1>This is user page</h1>
      <button onClick={()=>setSearchParams({users:"Active"})}>Active users</button>
      <button onClick={()=>setSearchParams({})}>Reset users</button>
      {Activeusers ? (<><li>user1</li> <li>user2</li></>)
      :(<><li>user1</li> <li>user2</li> <li>user3</li> <li>user4</li> <li>user5
        </li></>)
    }
    </div>
  )
}

export default User
