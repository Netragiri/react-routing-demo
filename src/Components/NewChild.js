import React from 'react'

function NewChild() {
  return (
    <div>
      <h1>new child component</h1>
    </div>
  )
}

export default NewChild
