import React from 'react'

function PageNotFound() {
  return (
    <div>
      <h3 style={{color:'red'}}>'Page not found'</h3>
    </div>
  )
}

export default PageNotFound
