import React from 'react'
import {Link, Outlet} from 'react-router-dom'

function Products() {
  return (
      <>
       <div>
      <ol>
          <li>Electronics</li>
          <li>Clothes</li>
          <li>Groccery</li>
          <li>Shoes</li>
      </ol>
    
      <div style={{margin:'30px'}}>
    <Link to='newProduct'>New products</Link><br />
    <Link to='oldProducts'>Old products</Link>
    </div>
    </div>
   
    <Outlet/>
      </>
   
  )
}

export default Products
